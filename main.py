# [START gae_python37_app]
import py_nextbus
from flask import *
import json
import lib
import pred
import traceback
import os


app = Flask(__name__)

key = os.urandom(16)
with open('secret_key.txt', 'w') as file:
  file.write(str(key))
app.secret_key = key

with open('./pred_db.json', 'r') as file:
  preds = json.load(file)

@app.route('/')
def index():
  return render_template('index.html')

@app.route('/about')
def about():
  return render_template('about.html')

@app.route('/hamster')
def hamster():
  return render_template('hamster.html')

@app.route('/pred/<a>/<r>/<i>/<d>')
def pred_a_r_d(a, r, i, d):
  data = _api_pred_time(a, r, i, d)
  return render_template('new_pred.html', )

@app.route('/hamster/new/<a>/<r>/<i>/<interval>')
def hamster_new_wait(a, r, i, interval):
  return render_template('hamster_new_redirect.html', url='/hamster/new/raw/{a}/{r}/{i}/{ivl}'
                         .format(a=a, r=r, i=i, ivl=interval))

def _api_pred_dist(a, r, i, ivl):
  with open('./realtime.json', 'r') as file:
    raw_data = json.load(file)
  if a not in raw_data.keys():
    return {'status':'error', 'code':'4041'}
  if r not in raw_data[a].keys():
    return {'status':'error', 'code':'4042'}
  if i not in raw_data[a][r].keys():
    return {'status':'error', 'code':'4043'}
  data = []
  temp_data = raw_data[a][r][i]
  for key in temp_data.keys():
    value = temp_data[key]
    data.append(float(value))
  del key, value, temp_data
  temp_data = data
  if len(data) > 20:
    if int(ivl)*20 > len(temp_data):
      return {'status':'error', 'code':'5002'}
    data = []
    for i in range(0, 20*int(ivl), int(ivl)):
      data.append(temp_data[i])
  if len(data) != 20:
    return {'status':'error', 'code':'5001'}
  inst = pred.predder()
  new_data, raw_data, result = inst.eval(data, history=True)
  data.append(len(temp_data))
  return {'status': 'ok', 'code': '200', 'raw_data': list(data), 'result': result.tolist()}
  # return {'status': 'ok', 'code': '200', 'raw_data': list(data), 'result': list(result)}

def _api_error(code = '500'):
  return {'status': 'error', 'code': str(code)}

def _api_pred_time(a, r, i, d):
  data_1 = _api_pred_dist(a, r, i, 1)
  data_3 = _api_pred_dist(a, r, i, 3)
  if data_1['status'] == 'error' or data_3['status'] == 'error':
    return _api_error('5005-{}-{}'.format(data_1['code'], data_3['code']))
  y_values = [data_1['result'][0][0], data_3['result'][0][0]]
  # x_values = [1, 3]
  # slope = (y_values[0] - y_values[1]) / (x_values[0] - x_values[1])
  slope = (float(y_values[0]) - float(y_values[1])) / (-2) # since always 1-3, set to -2 hardcode
  t = float(d) / slope
  return {'status': 'ok', 'code': '200', 'result': t}

def _api_list(t, a=None, r=None):
  with open('./realtime.json', 'r') as file:
    data = json.load(file)
  if t == 'a':
    return {'status': 'ok', 'code': '200', 't': t, 'result': list(data.keys())}
  if t == 'r':
    return {'status': 'ok', 'code': '200', 't': t, 'a': a, 'result': list(data[a].keys())}
  if t == 'b':
    return {'status': 'ok', 'code': '200', 't': t, 'a': a, 'r': r, 'result': list(data[a][r].keys())}
  return _api_error('4002')

@app.route('/api/<req_name>')
def api(req_name):
  if req_name == 'pred_dist':
    try:
      a = request.args.get('a')
      r = request.args.get('r')
      i = request.args.get('i')
      ivl = request.args.get('ivl')
      return json.dumps(_api_pred_dist(a, r, i, ivl))
    except Exception:
      print(traceback.format_exc())
      return json.dumps(_api_error(code = '5003'))
  elif req_name == 'pred_time':
    try:
      a = request.args.get('a')
      r = request.args.get('r')
      i = request.args.get('i')
      d = request.args.get('d')
      return json.dumps(_api_pred_time(a, r, i, d))
    except Exception:
      print(traceback.format_exc())
      return json.dumps(_api_error(code = '5004'))
  elif req_name == 'list':
    try:
      t = request.args.get('t')
      a = request.args.get('a')
      r = request.args.get('r')
      return json.dumps(_api_list(t, a, r))
    except Exception:
      print(traceback.format_exc())
      return json.dumps(_api_error(code = '5004'))
  else:
    return json.dumps(_api_error(code = '4001'))

@app.route('/ui/<req_name>')
def ui(req_name):
  api_result = api(req_name)
  api_result = json.loads(api_result)
  if api_result['status'] != 'ok' and api_result['code'] != '200':
    return render_template('error.html', code=api_result['code'], short='API',
                           long='API returned error code {}: {}'.format(api_result['code'], api_result))
  if req_name == 'pred_time':
    return render_template('hamster_new_pred.html', result=api_result['result'], debug_data=api_result)
  elif req_name == 'pred_dist':
    return render_template('hamster_new_pred.html', result = api_result['result'][0][0], debug_data = api_result)
  elif req_name == 'list':
    return render_template('hamster_new_list.html', result = api_result['result'], debug_data = api_result)
  else:
    return render_template('error.html', code='4046', short='req_name not found.',
                           long='Requested API command name not found (i.e. else stat).')

@app.route('/hamster/new/raw/<a>/<r>/<i>/<interval>')
def hamster_new(a, r, i, interval):
  with open('./realtime.json', 'r') as file:
    raw_data = json.load(file)
  if a not in raw_data.keys():
    return render_template('error.html', code='4041', short='Agency not found.',
                           long = 'Agency {a} not collected by live daemon, '
                                  'so not found.<hr>{data}'.format(a=a, data=raw_data.keys()))
  if r not in raw_data[a].keys():
    return render_template('error.html', code = '4042', short = 'Agency not found.',
                           long = 'Route {r} of agency {a} not collected by live daemon, '
                                  'so not found.<hr>{data}'.format(a=a, r=r, data=raw_data[a].keys()))
  if i not in raw_data[a][r].keys():
    return render_template('error.html', code = '4043', short = 'Bus ID not found.',
                           long = 'Bus ID (serial number) {i} of route {r} of agency {a} '
                                  'not collected by live daemon, '
                                  'so not found.<hr>{data}'.format(a=a, r=r, i=i, data=raw_data[a][r].keys()))
  data = []
  temp_data = raw_data[a][r][i]
  for key in temp_data.keys():
    value = temp_data[key]
    data.append(float(value))
  del key, value, temp_data
  temp_data = data
  if len(data) > 20:
    if int(interval)*20 > len(temp_data):
      return render_template('error.html', code = '5002', short = 'Server not ready.',
                             long = 'Data len not enough. May be malfunction. Data (all) len is {}'.format(len(temp_data)))
    data = []
    for i in range(0, 20*int(interval), int(interval)):
      data.append(temp_data[i])
  if len(data) != 20:
    return render_template('error.html', code = '5001', short = 'Server not ready.',
                           long = 'Data len not equal to 20. May be malfunction. Data (all) len is {}'.format(len(temp_data)))
  inst = pred.predder()
  new_data, raw_data, result = inst.eval(data, history=True)
  data.append(len(temp_data))
  return render_template('hamster_new.html', new_data=new_data, raw_data=data, result=result)

@app.route('/hamster/new/dbg/<float:data_0>/<float:data_1>/<float:data_2>/<float:data_3>/<float:data_4>/<float:data_5>/<float:data_6>/<float:data_7>/<float:data_8>/<float:data_9>/<float:data_10>/<float:data_11>/<float:data_12>/<float:data_13>/<float:data_14>/<float:data_15>/<float:data_16>/<float:data_17>/<float:data_18>/<float:data_19>')
def hamster_new_raw(data_0, data_1, data_2, data_3, data_4, data_5, data_6, data_7, data_8, data_9, data_10, data_11, data_12, data_13, data_14, data_15, data_16, data_17, data_18, data_19, ):
  data = [data_0, data_1, data_2, data_3, data_4, data_5, data_6, data_7, data_8, data_9, data_10, data_11, data_12, data_13, data_14, data_15, data_16, data_17, data_18, data_19, ]
  inst = pred.predder()
  new_data, raw_data, result = inst.eval(data, history=True)
  return render_template('hamster_new.html', new_data=new_data, raw_data=raw_data, result=result)

@app.route('/hamster/raw/<sid>/<time>/<dist>')
def predict_raw(sid, time, dist):
  return json.dumps(lib._predict(sid, time, dist))
# <iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox={{ raw_lat-0.005 }}%2C{{ raw_lon }}%2C-79.27030026912689%2C43.732499358244155&amp;layer=mapnik&amp;marker=43.731255086677066%2C-79.27265256643295" style="border: 0px;">Map is not displayed. Allow iframe to display map.</iframe>


@app.route('/hamster/sid/<sid>/<time>/<dist>')
def predict(sid, time, dist):
  result = lib._predict(sid, time, dist)
  if result == 1:
    return render_template('error.html', code='500-1', short='Unsupported SID',
                           long='SID is not supported.')
  return render_template('pred.html', sid=sid, time=time, dist=dist, wait_time=result[1], now_dist=result[2],
                         debug=result['debug'], raw_time=result['raw_time'], raw_lon=result[4], raw_lat=result[5],
                         raw_dist=result[6], vehicle=result['vehicle'])

@app.route('/hamster/min/<sid>/<time>/<dist>')
def predict_min(sid, time, dist):
  result = lib._predict(sid, time, dist)
  if result == 1:
    return render_template('error.html', code='500-1', short='Unsupported SID',
                           long='SID is not supported.')
  return render_template('pred_min.html', sid=sid, time=time, dist=dist, wait_time=result[1], now_dist=result[2],
                         debug=result['debug'], raw_time=result['raw_time'], raw_lon=result[4], raw_lat=result[5],
                         raw_dist=result[6], vehicle=result['vehicle'])

@app.route('/hamster/scrollable/<int:n_preds>')
def predict_scrollable(n_preds):
  urls = []
  for i in range(n_preds):
    # Pr(ediction) ID
    prid = [request.args.get('sid{}'.format(i)), request.args.get('time{}'.format(i)), request.args.get('dist{}'.format(i))]
    print(prid)
    urls.append('/hamster/min/{}/{}/{}'.format(*prid))
  del i, prid
  return render_template('scrollable.html', urls = urls)

@app.errorhandler(404)
def error404(error):
  return render_template('error.html', code='404', short='Route not found.',
                         long='There were no responses for URL "{}".'.format(request.url))

@app.errorhandler(500)
def error500(error):
  return render_template('error.html', code='500', short='Internal Error',
                         long='Error encountered while processing request.')

@app.errorhandler(403)
def error403(error):
  return render_template('error.html', code='403', short='Forbidden',
                         long='Unable to access forbidden route.')

@app.errorhandler(410)
def error410(error):
  return render_template('error.html', code='410', short='Gone',
                         long='Unable to access gone route.')


if __name__ == '__main__':
  from gevent.pywsgi import WSGIServer
  http_server = WSGIServer(('0.0.0.0', 80), app)
  http_server.serve_forever()
  # app.run(host='0.0.0.0', port=8080, debug=False, use_evalex=False)
# [END gae_python37_app]
