if [ "$EUID" -ne 0 ]
  then echo "gce_startup.sh MUST be run as ROOT."
  exit
fi

# Install Stackdriver logging agent
curl -sSO https://dl.google.com/cloudagents/install-logging-agent.sh
sudo bash install-logging-agent.sh

# Install or update needed software
echo "Update software"
sudo apt update
sudo apt upgrade -y
sudo apt autoremove -y
apt-get install -y git supervisor python python-pip python3 python3-pip gunicorn3 lsof
pip install --upgrade pip virtualenv
python3 -m pip install --upgrade pip virtualenv

# Kill other programs running on port
pid=$(lsof -t -i :80 -s TCP:LISTEN)   # should be the port running
kill '$pid'
pid=$(lsof -t -i :8080 -s TCP:LISTEN) # just in case
kill '$pid'

# PARTIALLY Remove old artifacts
sudo rm -r /opt/app
# Account to own server process
/usr/sbin/useradd -m -d /home/pythonapp pythonapp

# Fetch source code
export HOME=/root
echo "GIT CLONE"
echo "GIT CLONE"
echo "GIT CLONE"

git clone https://gitlab.com/transference/hamster/hamster-web.git /opt/app/hamster-web
(cd /opt/app/hamster-web && git pull)

# Python environment setup
virtualenv -p python3 /opt/app/hamster-web/env
source /opt/app/hamster-web/env/bin/activate
/opt/app/hamster-web/env/bin/pip install grpcio
/opt/app/hamster-web/env/bin/pip install -r /opt/app/hamster-web/requirements.txt

# Set ownership to newly created account
chown -R pythonapp:pythonapp /opt/app

# Put supervisor configuration in proper place
cp /opt/app/hamster-web/python-app.conf /etc/supervisor/conf.d/python-app.conf

# Start service via supervisorctl
# sudo unlink /var/run/supervisor.sock
# sudo unlink /tmp/supervisor.sock
# supervisorctl reread
# supervisorctl update
# supervisord
echo "RUN"
bash gce_run_novenv.sh

# Start No-IP DUC
sudo /usr/local/bin/noip2 &
