import time

import py_nextbus
import json
import geopy.distance


# import requests


# def len_ors(coords_0, coords_1):
#   '''WORK IN PROGRESS!!! DO NOT USE'''
#   url_format = 'https://api.openrouteservice.org/v2/directions/driving-car?api_key={}&start={},{}&end={},{}'
#   request = requests.get(url_format)
#   if str(request.status_code[0]) != '2':
#     raise RuntimeError('Request return status code not 200: {}'.format(request.status_code))
#   data = json.loads(request.text)

def read_config(path = './config.json'):
  with open(path, 'r') as file:
    return json.load(file)


config = read_config()
before_times = list()
for i in range(config['daemon_exec_num']):
  before_times.append(0)
client = py_nextbus.client.NextBusClient(output_format = 'json')
out_data, min_data = {}, {}
time_limit = config['daemon_iter_time_limit']
data_mode = 'min' # 'min' or 'all'

run = True

print('start {}'.format(time.time()))
h = 0
# time_next = 0
while run:
  time_start = time.time()
  print('├loop {}, {}'.format(h, time_start))
  max_len = config['daemon_max_len']
  for i in range(len(config['daemon_agency_tags'])):
    agency = config['daemon_agency_tags'][i]
    print('├┬agency {}, {}'.format(i, agency))
    for k in range(len(config['daemon_route_tags'])):
      try:
        route = config['daemon_route_tags'][k]
        print('│├route {}, {}'.format(k, route))
        route_start = config['daemon_route_start_latlon'][route]
        try:
          data = client.get_vehicle_locations(route, before_times[i], agency)
        except Exception as err:
          print('{}: {}'.format(type(err), err))
        before_times[i] = int(time.time())
        get_time = float(time.time())
        if 'vehicle' not in data:
          continue
        data = data['vehicle']
        temp = []
        for j in range(len(data)):
          vehicle = data[j]
          skip = False
          req_keys = ['id', 'routeTag', 'dirTag', 'heading', 'secsSinceReport', 'lat', 'lon', 'id', 'speedKmHr']
          for req_key in req_keys:
            if req_key not in vehicle: skip = True
          if skip: continue
          new_vehicle = {}
          vehicle_latlon = [float(vehicle['lat']), float(vehicle['lon'])]
          new_vehicle['dist'] = geopy.distance.distance(route_start[::-1], vehicle_latlon[::-1]).km
          new_vehicle['time'] = get_time - int(vehicle['secsSinceReport'])
          if data_mode == 'all':
            if agency not in out_data.keys():
              out_data[agency] = {}
            if route not in out_data[agency].keys():
              out_data[agency][route] = {}
            if vehicle['id'] not in out_data[agency][route].keys():
              out_data[agency][route][vehicle['id']] = []
            new_vehicle['id'] = vehicle['id']
            new_vehicle['route'] = vehicle['routeTag']
            new_vehicle['hdg'] = int(vehicle['heading'])
            new_vehicle['loc'] = vehicle_latlon
            new_vehicle['sloc'] = route_start
            new_vehicle['speed'] = int(vehicle['speedKmHr'])
            out_data[agency][route][vehicle['id']].append(new_vehicle)
            keys = list(out_data[agency][route][vehicle['id']].keys())
            if len(keys) > max_len:
              out_data.pop(min(keys), None)
          elif data_mode == 'min':
            if agency not in min_data.keys():
              min_data[agency] = {}
            if route not in min_data[agency].keys():
              min_data[agency][route] = {}
            if vehicle['id'] not in min_data[agency][route].keys():
              min_data[agency][route][vehicle['id']] = {}
            min_data[agency][route][vehicle['id']][int(new_vehicle['time'])] = new_vehicle['dist']
            keys = list(min_data[agency][route][vehicle['id']].keys())
            if len(keys) > max_len and min(keys) in keys:
              min_data[agency][route][vehicle['id']].pop(min(keys), None)
      except Exception as err:
        print(type(err), err)
  h += 1
  config = read_config()
  with open('./realtime.json', 'w') as file:
    json.dump(min_data, file, indent = 2)
  time_end = time.time()
  time_wait = time_limit - time_end + time_start
  # time_comp = False
  # if time_wait < 0:
  #   time_next = time_wait
  #   time_wait = 0
  # elif time_wait > 0 and time_next < 0:
  #   if time_next <= time_wait:
  #     time_next_temp = time_next
  #     time_next += time_wait - time_next
  #     time_wait -= time_next_temp
  #     del time_next_temp
  #   elif time_next > time_wait:
  #     time_next += time_wait
  #     time_wait = 0
  #   time_wait = 0
  #   time_comp = True
  # print('├wait {}, {}, {}'.format({True: 'override', False: 'realtime'}[time_comp], time_wait, time_next))
  print('├wait {}'.format(time_wait))
  if time_wait < 0: time_wait = 0
  time.sleep(time_wait)

# print(json.dumps(out_data, indent = 2))
# print(json.dumps(min_data, indent = 2))
