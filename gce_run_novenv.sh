echo "GCE Run Shell Script NO VENV"
echo "Only use it for GCE stuff with custom sh script!!"


cd /opt/app/hamster-web
python3 -m pip install grpcio # see https://github.com/grpc/grpc/issues/12992
python3 -m pip install -r requirements.txt

# export FLASK_APP='/opt/app/hamster-web/main.py:app'
# sudo python3 -m flask run
sudo gunicorn3 -w 4 -b 0.0.0.0:80 "main:app"

echo "Called/Done."
