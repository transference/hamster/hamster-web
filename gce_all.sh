if [ "$EUID" -ne 0 ]
  then echo "gce_all.sh MUST be run as ROOT."
  exit
fi

echo "-----------------------------"
echo "gce_all.sh for Hamster Web v8"
echo "-----------------------------"

echo "-----------------------------"
echo "Install/Update/Autoremove Software"
echo "Stackdriver logging agent"
echo "-----------------------------"

curl -sSO https://dl.google.com/cloudagents/install-logging-agent.sh
sudo bash install-logging-agent.sh

echo "-----------------------------"
echo "Install/Update/Autoremove Software"
echo "apt"
echo "-----------------------------"

echo "apt update"
sudo apt update

echo "apt upgrade"
sudo apt upgrade -y

echo "apt autoremove"
sudo apt autoremove -y

echo "apt install"
apt-get install -y git supervisor python python-pip python3 python3-pip gunicorn3 lsof gevent

echo "-----------------------------"
echo "Install/Update/Autoremove Software"
echo "pip"
echo "-----------------------------"

echo "pip install"
pip install --upgrade pip virtualenv

echo "python3 -m pip install"
python3 -m pip install --upgrade pip virtualenv

echo "-----------------------------"
echo "Clean environ"
echo "term processes using ports 80, 8080"
echo "-----------------------------"

echo "80"
pid=$(lsof -t -i :80 -s TCP:LISTEN)   # should be the port running
kill '$pid'

echo "8080"
pid=$(lsof -t -i :8080 -s TCP:LISTEN) # just in case
kill '$pid'

echo "-----------------------------"
echo "Clean old artifacts"
echo "/opt/app"
echo "-----------------------------"
sudo rm -r /opt/app

echo "-----------------------------"
echo "Add User"
echo "add default user to own server process"
echo "-----------------------------"
/usr/sbin/useradd -m -d /home/pythonapp pythonapp

echo "-----------------------------"
echo "Set env vars"
echo "HOME=/root"
echo "-----------------------------"
export HOME=/root

echo "-----------------------------"
echo "Get source code"
echo "git"
echo "-----------------------------"

echo "git clone"
git clone https://gitlab.com/transference/hamster/hamster-web.git /opt/app/hamster-web

echo "cd && git pull"
(cd /opt/app/hamster-web && git pull)

# echo "-----------------------------"
# echo "Setup Py3 environ"
# echo "venv"
# echo "-----------------------------"

# echo "venv"
# virtualenv -p python3 /opt/app/hamster-web/env

# echo "activate"
# source /opt/app/hamster-web/env/bin/activate

echo "-----------------------------"
echo "Install/Update/Autoremove Software"
echo "pip"
echo "-----------------------------"

echo "pip install grpcio"
/opt/app/hamster-web/env/bin/pip install grpcio

echo "pip install -r requirements.txt"
/opt/app/hamster-web/env/bin/pip install -r /opt/app/hamster-web/requirements.txt

echo "-----------------------------"
echo "Chown file to pythonapp usrr"
echo "chown"
echo "-----------------------------"
chown -R pythonapp:pythonapp /opt/app

echo "-----------------------------"
echo "Supervisor Config"
echo "cp"
echo "-----------------------------"
cp /opt/app/hamster-web/python-app.conf /etc/supervisor/conf.d/python-app.conf

# echo "-----------------------------"
# echo "Supervisor Run"
# echo "supervisorctl"
# echo "-----------------------------"
# Start service via supervisorctl
# sudo unlink /var/run/supervisor.sock
# sudo unlink /tmp/supervisor.sock
# supervisorctl reread
# supervisorctl update
# supervisord

echo "-----------------------------"
echo "Install/Update/Autoremove Software"
echo "pip"
echo "-----------------------------"
cd /opt/app/hamster-web
python3 -m pip install grpcio # see https://github.com/grpc/grpc/issues/12992
python3 -m pip install flask gevent py-nextbus requests
python3 -m pip install -r requirements.txt

echo "-----------------------------"
echo "Set var"
echo "flask run"
echo "-----------------------------"
cd /opt/app/hamster-web
python3 /opt/app/hamster-web/main.py
# sudo (export FLASK_APP='/opt/app/hamster-web/main.py:app' && python3 -m flask run --host=0.0.0.0 --port=80)
# sudo gunicorn3 -w 4 -b 0.0.0.0:80 "main:app"

echo "-----------------------------"
echo "DDNS"
echo "No-IP DUC"
echo "-----------------------------"

# Start No-IP DUC
sudo /usr/local/bin/noip2 &
