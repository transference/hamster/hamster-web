import tensorflow as tf
from tensorflow import keras
import json
import copy


class predder():
  def __init__(self, config_path='./config.json', ):
    self.config_path = config_path
    self.config      = self._load_config(self.config_path)
    self.etc_config  = self._load_config(self.config['config_path'])
    self.model       = keras.models.load_model(self.config['model_path'])
    
  def _load_config(self, path=None):
    if not path: path = self.config_path
    with open(path, 'r') as file:
      return json.load(file)

  def _stan(self, data):
    return (data - self.etc_config[0]) / self.etc_config[1]

  def _destan(self, data):
    return (data * self.etc_config[1]) + self.etc_config[0]
  
  def _change_shape(self, data):
    result = [[[]]]
    for i in range(len(data)):
      value = data[i]
      result[0][0].append([value])
    return result
  
  def _map_stan(self, data):
    for i in range(len(data[0][0])):
      data[0][0][i] = [self._stan(data[0][0][i][0])]
    return data
  
  def _eval(self,
            data = [2.07, 2.02, 2.0, 1.95, 1.92, 1.90, 1.80, 1.70, 1.60, 1.57, 1.53, 1.43, 1.35, 1.29, 1.23, 1.17, 1.12, 1.0, 0.9, 0.85, ],
            history=False):
    data     = self._change_shape(data)
    raw_data = copy.deepcopy(data)
    data     = self._map_stan(data)
    check    = tf.data.Dataset.from_tensor_slices(data)
    result   = self.model.predict(check)
    result   = self._destan(result)
    if history:
      new_data = raw_data[0][0]
      new_data.append(result)
      return (new_data, raw_data, result)
    else:
      return result
  
  def eval(self, data, history=False):
    assert isinstance(data, list)
    if len(data) != 20:
      raise RuntimeError('Arg "data" len must be 20; actually {}.'.format(len(data)))
    assert isinstance(history, bool)
    return self._eval(data, history)


if __name__ == '__main__':
  inst = predder()
  inp = input('Enter Data (sep by space) (None for default data):\n>')
  if inp == "":
    data = [2.07, 2.02, 2.0, 1.95, 1.92, 1.90, 1.80, 1.70, 1.60, 1.57, 1.53, 1.43, 1.35, 1.29, 1.23, 1.17, 1.12, 1.0, 0.9, 0.85, ]
  else:
    data = inp.split(' ')
  new_data, raw_data, result = inst.eval(data, history=True)
  print('Results:')
  print('New:  {}'.format(new_data))
  print('Raw:  {}'.format(raw_data))
  print('Pred: {}'.format(result))